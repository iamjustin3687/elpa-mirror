ytdl.el is an Emacs-based interface for youtube-dl, written in
emacs-lisp.

youtube-dl is a command-line program to download videos from
YouTube.com and a few more sites.  More information, at
https://github.com/ytdl-org/ytdl/blob/master/README.md#readme.

* Setup

Add "(require 'ytdl)" to your "init.el" file

Further customization can be found in the documentation online.
